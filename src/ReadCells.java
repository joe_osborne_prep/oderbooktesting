import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public abstract class ReadCells {

    public static class subOrder implements Comparable<subOrder>{

        public int id = 0;
        public int volume = 0;
        public double price = 0.0;
        public String direction = "";

        public subOrder(int ids, int volumes, double prices, String directions) {

            id = ids;
            volume = volumes;
            price = prices;
            direction = directions;
        }

        public static class OrderByPriceMin implements Comparator<subOrder> {

            @Override
            public int compare(subOrder o1, subOrder o2) {
                return o1.price > o2.price ? 1 : (o1.price < o2.price ? -1 : 0);
            }
        }

        public static class OrderByPriceMax implements Comparator<subOrder> {

            @Override
            public int compare(subOrder o1, subOrder o2) {
                return o1.price < o2.price ? 1 : (o1.price > o2.price ? -1 : 0);
            }
        }

        @Override
        public int compareTo(subOrder o) {
            return 0;
        }
    }

    public static ArrayList<subOrder> buyOrders = new ArrayList<>();
    public static ArrayList<subOrder> sellOrders = new ArrayList<>();
    public static ArrayList<String> completeOrders = new ArrayList<>();
    public static ArrayList<String> afterCompleteOrders = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("BUYOrders.txt"));
            String read = null;
            while ((read = in.readLine()) != null) {
                read = read.replaceAll("\\s", "");
                String[] splited = read.split(",");

                if (splited.length > 2) {

                    subOrder x = new subOrder(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]), Double.valueOf(splited[2]), splited[3]);

                    if (x.direction.equals("BUY")) {
                        buyOrders.add(x);
                        Collections.sort(buyOrders, new subOrder.OrderByPriceMax());
                    } else {
                        sellOrders.add(x);
                        Collections.sort(sellOrders, new subOrder.OrderByPriceMin());

                    }
                }
                try {
                    double topBuy = buyOrders.get(0).price;
                    try {
                        double topSell = sellOrders.get(0).price;

                        if(topBuy >= topSell) {
                            System.out.println("MATCH!!!-----------------------------------------------");
                            //possible class?
                            subOrder a = buyOrders.get(0);
                            subOrder b = sellOrders.get(0);

                            int stocka = a.volume;
                            int stockb = b.volume;
                            int newStocka = 0;
                            int newStockb = 0;

                            if (stocka>=stockb){
                                newStocka = stocka-stockb;
                                subOrder newSub = new subOrder(a.id,newStocka,a.price,a.direction);
                                buyOrders.remove(0);
                                sellOrders.remove(0);
                                if(newStocka <= 0){}
                                else{
                                    buyOrders.add(0,newSub);
                                }
                            }
                            else if(stockb>=stocka){
                                newStockb = stockb-stocka;
                                subOrder newSub = new subOrder(b.id,newStockb,b.price,b.direction);
                                buyOrders.remove(0);
                                sellOrders.remove(0);
                                if(newStockb <= 0){}
                                else{
                                    sellOrders.add(0,newSub);
                                }
                            }
                            String comp = String.valueOf(a.id+" : "+a.volume+" : "+a.price+" : "+a.direction+" || "+b.direction+" : "+b.price+" : "+b.volume+" : "+b.id);
                            completeOrders.add(comp);

                            String aComp = String.valueOf(a.id+" : "+newStocka+" : "+a.price+" : "+a.direction+" || "+b.direction+" : "+b.price+" : "+newStockb+" : "+b.id);
                            afterCompleteOrders.add(aComp);
                        }
                    }catch (IndexOutOfBoundsException e){}
                }
                catch (IndexOutOfBoundsException e){}
            }
        } catch (IOException e) {
            System.out.println("There was a problem: " + e);
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
        
        for(int q=0;q<completeOrders.size();q++){
            System.out.println("\n"+"COMPLETE-----------------");
            System.out.println("        ID   QTY   (£)         ||        (£)     QTY   ID");
            System.out.println("before: "+completeOrders.get(q));
            System.out.println("after:  "+afterCompleteOrders.get(q)+"\n");
        }

//        for (int x = 0; x < buyOrders.size(); x++) {
//            System.out.println("BUY---------------------");
//            System.out.println("order: " + (x + 1));
//            subOrder z = buyOrders.get(x);
//            System.out.println(z.id + " : " + z.volume + " : " + z.price + " : " + z.direction);
//
//        }
//
//        for (int x = 0; x < sellOrders.size(); x++) {
//            System.out.println("SELL---------------------");
//            System.out.println("order: " + x);
//            subOrder z = sellOrders.get(x);
//            System.out.println(z.id + " : " + z.volume + " : " + z.price + " : " + z.direction);
//
//        }


    }
}